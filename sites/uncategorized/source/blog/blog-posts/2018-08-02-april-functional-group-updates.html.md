---
title: "GitLab's Functional Group Updates - April"
author: Trevor Knudsen
author_gitlab: tknudsen
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on in April"
canonical_path: "/blog/2018/08/02/april-functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our GitLab Team call, a different Functional Group gives an update to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1hcBxv9pzRoTmBKMRRrJKW0TmiKB7rR7VWDNH0UQI7jk/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NTKT0bmvlIY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/1zfv90uU7iI6HAKKARb52jUz8gbdLNJjVb5T94FJm978/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/fKmrpCsfIL0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Distribution Team

[Presentation slides](https://docs.google.com/presentation/d/1Rksc03NP10CoH3E-Xw9FShMBJvx2OoQFvdOHIOheDiE/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/mNjDyaNJpyk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

----

### Engineering Team

[Presentation slides](https://docs.google.com/presentation/d/1kD_0ElQV9-FKFh37ln55tp4yFS0VSrpFq20VK_BBgOM/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/t2wbASPSvng" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Frontend Team

[Presentation slides](https://docs.google.com/presentation/d/1AWrloGXs3y5G3isujsEHPW6OTUIpRx6cIs74ny2zd3E/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/hiFbQDHB3xk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Monitoring Team

[Presentation slides](https://docs.google.com/presentation/d/11rNwmKPeLx7fLCydLjBEmXBJXuwfYCEnYE55pXKWp4w/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/JjMVnA0TuiM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Quality Team

[Presentation slides](https://docs.google.com/presentation/d/1a1fiw-464vuehiRlGCGbo2S2FtN9Ct5p3SfplvEFTSc/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/m8jHtDLdPFc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Discussion Team

[Presentation slides](http://gitlab-org.gitlab.io/group-conversations/backend-discussion/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EMfhicC152k" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----
